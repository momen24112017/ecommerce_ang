import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartComponent } from './cart/cart.component';

import { CategoryComponent } from './category/category.component';

import { CheckoutComponent } from './checkout/checkout.component';

import { IndexComponent } from './index/index.component';

import { ProductDetailComponent } from './product-detail/product-detail.component';

import { TestComponent } from './test/test.component';
import { TypeEmailRecoverPassComponent } from './type-email-recover-pass/type-email-recover-pass.component';
import { EmailSentToRecoverPassComponent } from './email-sent-to-recover-pass/email-sent-to-recover-pass.component';
import { ResetPassComponent } from './reset-pass/reset-pass.component';
import { VerifyYourMailToRegComponent } from './verify-your-mail-to-reg/verify-your-mail-to-reg.component';
import { SuccessfullRegComponent } from './successfull-reg/successfull-reg.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { LogRegComponent } from './log-reg/log-reg.component';

import { RegLogComponent } from './reg-log/reg-log.component';
import { ListCategoryComponent } from './list-category/list-category.component';





const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    {
      path: '',
      component: IndexComponent
    },

    {
      path: 'category/:id',
      component: CategoryComponent
    },
    {
      path: 'cart',
      component: CartComponent
    },
    {
      path: 'checkout',
      component: CheckoutComponent
    },
    {
      path: 'product-detail/:id',
      component: ProductDetailComponent
    },
    {
      path: 'log-reg',
      component: LogRegComponent
    },
    {
      path: 'list-category',
      component: ListCategoryComponent
    },
    {
      path: 'reg-log',
      component: RegLogComponent
    },


    {
      path: 'type-email-recover-pass',
      component: TypeEmailRecoverPassComponent
    },
    {
      path: 'email-sent-to-recover-pass',
      component: EmailSentToRecoverPassComponent
    },

    {
      path: 'reset-pass',
      component: ResetPassComponent
    },
    {
      path: 'verify-your-mail-to-reg',
      component: VerifyYourMailToRegComponent
    },

    {
      path: 'successfull-reg',
      component: SuccessfullRegComponent
    },
    {
      path: 'my-account',
      component: MyAccountComponent
    },
    {
      path: 'test',
      component: TestComponent
    },
  ], { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
