import { Component, Inject } from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';
import { DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private titleService: Title, private ecommerceService: EcommerceService, @Inject(DOCUMENT) private _document: HTMLDocument) { }

  title = "";
  BrendSetting = {
    title: "", banner_image:"", favicon: "", id:"", logo: "", our_story: "",
    social_links: {facebook: "", instagram: "", google: ""},
    theme_colour: {main_colour: "", sub_colour: ""}
  };

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

  ngOnInit(): void {
    
    this.ecommerceService.getBrendSetting().subscribe(res=>{
      this._document.getElementById('appFavicon').setAttribute('href', res.data.favicon);
      this.BrendSetting = res.data;
      this.title = this.BrendSetting.title;
      this.setTitle(this.title); 
    },ero=>{
      console.log('ero');
    })
    
  }

}
