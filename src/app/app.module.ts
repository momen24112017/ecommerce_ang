import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { IndexComponent } from './index/index.component';
import { IndexBodyComponent } from './index-body/index-body.component';
import { CartComponent } from './cart/cart.component';
import { CartBodyComponent } from './cart-body/cart-body.component';
import { CategoryComponent } from './category/category.component';
import { CategoryBodyComponent } from './category-body/category-body.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CheckoutBodyComponent } from './checkout-body/checkout-body.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductDetailBodyComponent } from './product-detail-body/product-detail-body.component';
import { TestComponent } from './test/test.component';
import { EcommerceService } from './services/ecommerce.service';
import { FormsModule } from '@angular/forms';
import { TypeEmailRecoverPassComponent } from './type-email-recover-pass/type-email-recover-pass.component';
import { EmailSentToRecoverPassComponent } from './email-sent-to-recover-pass/email-sent-to-recover-pass.component';
import { ResetPassComponent } from './reset-pass/reset-pass.component';
import { VerifyYourMailToRegComponent } from './verify-your-mail-to-reg/verify-your-mail-to-reg.component';
import { SuccessfullRegComponent } from './successfull-reg/successfull-reg.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { LogRegComponent } from './log-reg/log-reg.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { SecondNavComponent } from './second-nav/second-nav.component';
import { RegLogComponent } from './reg-log/reg-log.component';
import { FilterPipe } from './pipes/filter.pipe';
import { ListCategoryComponent } from './list-category/list-category.component';
import { ListCategoryBodyComponent } from './list-category-body/list-category-body.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RatingModule } from 'ngx-bootstrap/rating';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    IndexComponent,
    IndexBodyComponent,
    CartComponent,
    CartBodyComponent,
    CategoryComponent,
    CategoryBodyComponent,
    CheckoutComponent,
    CheckoutBodyComponent,
    ProductDetailComponent,
    ProductDetailBodyComponent,
    TestComponent,
    TypeEmailRecoverPassComponent,
    EmailSentToRecoverPassComponent,
    ResetPassComponent,
    VerifyYourMailToRegComponent,
    SuccessfullRegComponent,
    MyAccountComponent,
    LogRegComponent,
    SecondNavComponent,
    RegLogComponent,
    FilterPipe,
    ListCategoryComponent,
    ListCategoryBodyComponent,
   
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    BrowserModule, 
    NgxPaginationModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    SlickCarouselModule,
    NgbModule,
    RatingModule.forRoot(),
  ],
  providers: [
    EcommerceService,
    Title
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
