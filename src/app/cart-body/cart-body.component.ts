import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { EcommerceService } from '../services/ecommerce.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import {Router} from '@angular/router';

@Component({
  selector: 'app-cart-body',
  templateUrl: './cart-body.component.html',
  styleUrls: ['./cart-body.component.css']
})
export class CartBodyComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService,  private toastr: ToastrService, private spinner: NgxSpinnerService, private router: Router) { }
  
  cities: any[]
  cityDeliveryFees: any;
  @ViewChild('promocode') promocodeForm: NgForm;
  promocode:string;
  promocodeValue: number;

  cart: any;
  subtotal : number;
  user = this.ecommerceService.getuserData();
  isActive: any;
  activeMailFlag = 0;

  BrendSetting = {
    title: "", banner_image:"", favicon: "", id:"", logo: "", our_story: "", email:"", phone:"",
    social_links: {facebook: "", instagram: "", google: "", twitter:"", linkedin:""},
    theme_colour: {main_colour: "", sub_colour: ""},
  };

  order = {order_products:"",promocode:"", promocodeValue:0, user_id: 0, name: "", email: "", 
    phone:"", address:"", city:"", delivery_fees:0, total:0, subtotal:0};

  onChange(event){
    this.cityDeliveryFees = event.target.options[event.target.options.selectedIndex].value;
    this.order.delivery_fees = this.cityDeliveryFees;
    this.order.city = event.target.options[event.target.options.selectedIndex].text;
  }

  calculateSubtotal(){
    var total = 0;
    for(var i = 0; i < this.cart.length; i++){
      total = total + (this.cart[i].price * this.cart[i].quantity);
    }
    this.order.subtotal = total;
    return total;
  }

  activeYourMail(){
    localStorage.setItem('userEmail',JSON.stringify(this.user.email));
    // window.location.href="/verify-your-mail-to-reg";
    this.router.navigateByUrl('/verify-your-mail-to-reg');
  }

  checkout(){
    
    if(localStorage.getItem('ecommerceUserData') == null){
      this.toastr.error('Please login or register first', 'Error', {timeOut: 5000,});
      return;
    }

    this.ecommerceService.checkActive(this.user.id).subscribe(res=>{
      this.isActive = res.data[0].is_active;
      if(this.isActive != 1){
        this.toastr.error('Please check your mail and activate your account', 'Error', {timeOut: 5000,});
        this.activeMailFlag = 1;
        return;
      }
    },ero=>{
      console.log('ero');
    });
    
    if(this.cart.length == 0){
      this.toastr.error('Your Cart is Empty', 'Error', {timeOut: 5000,});
      return;
    }

    if(this.cityDeliveryFees == undefined){
      this.toastr.error('Please enter your city first', 'Error', {timeOut: 5000,});
      return;
    }

    localStorage.setItem('order',this.ecommerceService.encrypt(JSON.stringify(this.order)));
    // window.location.href="/checkout";
    this.router.navigateByUrl('/checkout');
  }

  CheckPromoCode(form: NgForm){
    this.spinner.show("promocodeSpinner");
    if(this.cityDeliveryFees == undefined){
      this.toastr.error('Please enter your city first', 'Error', {timeOut: 5000,});
      this.spinner.hide("promocodeSpinner");
      return;
    }
    this.promocode = this.promocodeForm.value.promocode;
    this.ecommerceService.checkPromoCode(this.promocode).subscribe(res=>{
      if(res.status.code == 5010){
        this.toastr.error('This promo code is expired', 'Error', {timeOut: 5000,});
        this.spinner.hide("promocodeSpinner");
        return;
      }
      if(res.status.code == 200){
        this.promocodeValue = res.data[0].value;
        this.toastr.success('Promo code is valid', 'Success', {timeOut: 5000,});
        this.order.promocode = this.promocode;
        this.order.promocodeValue = this.promocodeValue;
        this.spinner.hide("promocodeSpinner");
      }
    },ero=>{
      console.log('ero');
    })
  }

  removeProduct(product_id){
    var index = this.cart.findIndex(cart => cart.id == product_id);
    this.cart.splice(index, 1);
    this.subtotal = this.calculateSubtotal();
    localStorage.setItem('arrItem',this.ecommerceService.encrypt(JSON.stringify(this.cart)));
    this.ngOnInit();
    this.router.navigateByUrl('/cart');
  }

  ngOnInit(): void {
    
    this.cart = this.ecommerceService.getarrItem();
    
    this.subtotal = this.calculateSubtotal();
    this.ecommerceService.listDeliveryFees().subscribe(res=>{
      this.cities=res.data;
    },ero=>{
      console.log('ero');
    });

    this.ecommerceService.getBrendSetting().subscribe(res=>{
      this.BrendSetting = res.data;
      if(this.BrendSetting.our_story.length > 245){
        this.BrendSetting.our_story = this.BrendSetting.our_story.substr(0, 245)+'...';
      }
    },ero=>{
      console.log('ero');
    });

  }

}
