import { Component, OnInit } from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-category-body',
  templateUrl: './category-body.component.html',
  styleUrls: ['./category-body.component.css']
})
export class CategoryBodyComponent implements OnInit {

  constructor(private route: ActivatedRoute, private ecommerceService: EcommerceService, private router: Router) { }

  ProductsAttachedtoCategory: any[];
  categoryName: string;
  categoryImage: string;
  total: number;
  current_page: number;
  next_page: number;
  per_page: number;

  flag = 0;

  setProductId(product_id) {
    localStorage.setItem('product_id', product_id);
    // window.location.href="/product-detail";
    // this.router.navigateByUrl('/product-detail');
    this.router.navigate(['/product-detail', product_id]);
  }

  pageChanged(event: any): void {
    this.ecommerceService.pageChange(localStorage.getItem('category_id'), event).subscribe(res => {
      this.ProductsAttachedtoCategory = res.data.data;
      this.categoryName = res.categoryName[0].name;
      this.total = res.data.total;
      this.current_page = res.data.current_page;
      this.next_page = this.current_page + 1;
      this.per_page = res.data.per_page;
    }, ero => {
      console.log('ero');
    })
  }

  updateComponent() {
    this.flag = 1;
    this.ngOnInit();
  }
  count: number = 0;
  clickEventsubscription: Subscription;
  incrementCount() {
    this.count++;
  }
  category_id;
  ngOnInit(): void {
    console.log('category body');
    
    this.clickEventsubscription = this.ecommerceService.getClickEvent().subscribe(res => {
      // we call the endpoint here 
      console.log('suppose to be cat id ',res);
      this.category_id = res;
      this.ecommerceService.listProductAttachedtoCategory(this.category_id).subscribe(res=>{
        this.ProductsAttachedtoCategory = res.data.data;
        this.categoryName = res.categoryName[0].name;
        this.categoryImage = res.categoryImage;
        this.total = res.data.total;
        this.current_page = res.data.current_page;
        this.next_page = this.current_page + 1;
        this.per_page = res.data.per_page;
      })
    })

    if (this.flag == 1) {
      this.ecommerceService.listProductAttachedtoCategory(this.category_id).subscribe(res => {
        this.ProductsAttachedtoCategory = res.data.data;
        this.categoryName = res.categoryName[0].name;
        this.categoryImage = res.categoryImage;
        this.total = res.data.total;
        this.current_page = res.data.current_page;
        this.next_page = this.current_page + 1;
        this.per_page = res.data.per_page;
      }, ero => {
        console.log('ero');
      });
    } else {
      this.ecommerceService.listProductAttachedtoCategory(this.route.snapshot.params['id']).subscribe(res => {
        this.ProductsAttachedtoCategory = res.data.data;
        this.categoryName = res.categoryName[0].name;
        this.categoryImage = res.categoryImage;
        this.total = res.data.total;
        this.current_page = res.data.current_page;
        this.next_page = this.current_page + 1;
        this.per_page = res.data.per_page;
      }, ero => {
        console.log('ero');
      });
    }

  }

}
