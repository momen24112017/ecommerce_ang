import { Component, OnInit } from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService) { }

  categories: any[];

  ngOnInit(): void {
    console.log('catttttttttttttt');
    
    this.ecommerceService.getCategories().subscribe(res=>{
      this.categories=res.data;
      console.log('cate');
      
    },ero=>{
      console.log('errrrrro',ero);
    })
  }

}
