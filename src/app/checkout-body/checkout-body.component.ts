import { Component, OnInit, ViewChild } from '@angular/core';
import { EcommerceService } from '../services/ecommerce.service';
import { Form, NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import {Router} from '@angular/router';

@Component({
  selector: 'app-checkout-body',
  templateUrl: './checkout-body.component.html',
  styleUrls: ['./checkout-body.component.css']
})
export class CheckoutBodyComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService, private toastr: ToastrService, private spinner: NgxSpinnerService,  private router: Router) { }

  BrendSetting = {
    title: "", banner_image:"", favicon: "", id:"", logo: "", our_story: "", email:"", phone:"",
    social_links: {facebook: "", instagram: "", google: "", twitter:"", linkedin:""},
    theme_colour: {main_colour: "", sub_colour: ""},
  };

  cities: any[];

  @ViewChild('promocode') promocodeForm: NgForm;
  promocode:string;
  promocodeValue: number;

  cart = this.ecommerceService.getarrItem();
  user = this.ecommerceService.getuserData();
  order = this.ecommerceService.getOrder();

  CreateOrder(){
    this.spinner.show("orderSpinner");
    if(this.order.address == "" || this.order.address == null || this.order.phone == "" || 
    this.order.phone == null || this.order.city == "" || this.order.city == null){
      this.toastr.error('Add your address and your phone number', 'Error', {timeOut: 5000,});
      this.spinner.hide("orderSpinner");
      return;
    }
    this.order.total = this.order.subtotal + Number(this.order.delivery_fees) - Number(this.order.promocodeValue);
    this.order.order_products = this.cart;
    this.ecommerceService.createOrder(this.order).subscribe(res=>{
      if(res.status.code == 200){
        this.toastr.success('You placed your order successfully', 'Success', {timeOut: 5000,});
        localStorage.removeItem('order');
        localStorage.removeItem('arrItem');
        this.spinner.hide("orderSpinner");
        this.router.navigateByUrl('/');
        return;
      }
    });
    this.spinner.hide("orderSpinner");
  }

  setDeliveryFees(event){
    this.order.delivery_fees = event.target.options[event.target.options.selectedIndex].value;
    this.order.city = event.target.options[event.target.options.selectedIndex].text;
  }

  CheckPromoCode(form: NgForm){
    this.spinner.show("promocodeSpinner");
    
    this.promocode = this.promocodeForm.value.promocode;
    this.ecommerceService.checkPromoCode(this.promocode).subscribe(res=>{
      if(res.status.code == 5010){
        this.toastr.error('This promo code is expired', 'Error', {timeOut: 5000,});
        this.spinner.hide("promocodeSpinner");
        return;
      }
      if(res.status.code == 200){
        this.promocodeValue = res.data[0].value;
        this.toastr.success('Promo code is valid', 'Success', {timeOut: 5000,});
        this.order.promocode = this.promocode;
        this.order.promocodeValue = this.promocodeValue;
        this.spinner.hide("promocodeSpinner");
      }
    },ero=>{
      console.log('ero');
    })
  }
 
  ngOnInit(): void {
    
    this.order.user_id = this.user.id;
    this.order.name = this.user.name;
    this.order.email = this.user.email;
    this.order.address = this.user.address;
    this.order.phone = this.user.phone;
    if(this.user.phone == "NULL"){
      this.order.phone = "";
    }
    if(this.user.address == "NULL"){
      this.order.address = "";
    }
    if(this.user.city == "NULL"){
      this.order.city = "";
    }

    this.ecommerceService.listDeliveryFees().subscribe(res=>{
      this.cities=res.data;
    },ero=>{
      console.log('ero');
    });

    this.ecommerceService.getBrendSetting().subscribe(res=>{
      this.BrendSetting = res.data;
      if(this.BrendSetting.our_story.length > 245){
        this.BrendSetting.our_story = this.BrendSetting.our_story.substr(0, 245)+'...';
      }
    },ero=>{
      console.log('ero');
    });
    
  }

}
