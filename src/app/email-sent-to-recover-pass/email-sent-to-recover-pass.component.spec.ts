import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailSentToRecoverPassComponent } from './email-sent-to-recover-pass.component';

describe('EmailSentToRecoverPassComponent', () => {
  let component: EmailSentToRecoverPassComponent;
  let fixture: ComponentFixture<EmailSentToRecoverPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailSentToRecoverPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailSentToRecoverPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
