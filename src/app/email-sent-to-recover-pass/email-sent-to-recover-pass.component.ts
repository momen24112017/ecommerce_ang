import { Component, OnInit } from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';

@Component({
  selector: 'app-email-sent-to-recover-pass',
  templateUrl: './email-sent-to-recover-pass.component.html',
  styleUrls: ['./email-sent-to-recover-pass.component.css']
})
export class EmailSentToRecoverPassComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService) { }

  successMessage:string

  ResendMailforResetPassword(){
    this.ecommerceService.resendMailforResetPassword(localStorage.getItem('userEmail')).subscribe(res=>{
      if(res.status.code == 200){
        this.successMessage = "Message sent successfully please check your mail";        
      }
    })
  }
  
  ngOnInit(): void {
  }

}
