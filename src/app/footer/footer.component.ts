import { Component, OnInit } from '@angular/core';
import { EcommerceService } from '../services/ecommerce.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService, private router: Router) { }

  BrendSetting = {title: "", banner_image:"", favicon: "", id:"", logo: "", our_story: "", email:"", phone:"",
    social_links: {facebook: "", instagram: "", google: "", twitter:"", linkedin:""},
    theme_colour: {main_colour: "", sub_colour: ""},
  };
  categories: any[];
  setCategoryId(Category){
    // localStorage.setItem('Category',Category.id);
    this.router.navigate(['/category', Category.id]);
    this.ecommerceService.sendClickEvent(Category.id);
    console.log('',Category.id);
    
  }

  ngOnInit(): void {

    this.ecommerceService.getCategories().subscribe(res=>{
      this.categories=res.data;
    },ero=>{
      console.log('ero');
    });

    this.ecommerceService.getBrendSetting().subscribe(res=>{
      this.BrendSetting = res.data;
      if(this.BrendSetting.our_story.length > 245){
        this.BrendSetting.our_story = this.BrendSetting.our_story.substr(0, 245)+'...';
      }
    },ero=>{
      console.log('ero');
    });

  }

}
