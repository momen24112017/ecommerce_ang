import { Component, OnInit, ViewChild } from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';
import {Router} from '@angular/router';
import { SlickCarouselComponent } from "ngx-slick-carousel";

@Component({
  selector: 'app-index-body',
  templateUrl: './index-body.component.html',
  styleUrls: ['./index-body.component.css'],
})
export class IndexBodyComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService, private router: Router) {}

  @ViewChild('slickModal', { static: true }) slickModal: SlickCarouselComponent;

  public product_id: Number;
  categories: any[];
  featureProducts: any[];
  BrendSetting = {title: "", banner_image:"", favicon: "", id:"", logo: "", our_story: "", email:"", phone:"",
    social_links: {facebook: "", instagram: "", google: "", twitter:"", linkedin:""},
    theme_colour: {main_colour: "", sub_colour: ""},
  };

  
  setProductId(product_id){
    localStorage.setItem('product_id',product_id);
    this.router.navigate(['/product-detail', product_id]);
    // this.router.navigateByUrl('/product-detail');
  }
  
  setCategoryId(category_id){
    localStorage.setItem('category_id',category_id);
    this.router.navigate(['/category', category_id]);
  }
  
  next() {
    this.slickModal.slickNext();
  }
  prev() {
    this.slickModal.slickPrev();
  }
  
  slideConfig = {
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    autoplaySpeed: 3000,
    speed: 1500,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  

  ngOnInit(): void {

    this.ecommerceService.getFeatureProducts().subscribe(res=>{
      this.featureProducts=res.data;
    },ero=>{
      console.log('ero');
    });

    this.ecommerceService.getCategories().subscribe(res=>{
      this.categories=res.data;
    },ero=>{
      console.log('ero');
    });

    this.ecommerceService.getBrendSetting().subscribe(res=>{
      this.BrendSetting = res.data;
    },ero=>{
      console.log('ero');
    });
 
  }

}
