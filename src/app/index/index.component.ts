import { Component, OnInit } from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService) { }

  loadingEffect = false;

  BrendSetting = {banner_image:"", favicon: "", id:"", logo: "", our_story: "",
  social_links: {facebook: "", instagram: "", google: ""},
  theme_colour: {main_colour: "", sub_colour: ""},
  title: ""};

  getLoadingEffectValue(){
    if(this.BrendSetting.banner_image != ""){
      this.loadingEffect = true;
      // setTimeout( ()=>{
      //   this.loadingEffect = true
      // }, 3000)
    }
  }

  ngOnInit(): void {

    this.ecommerceService.getBrendSetting().subscribe(res=>{
      this.BrendSetting = res.data;
      this.getLoadingEffectValue();
    },ero=>{
      console.log('ero');
    });

  }

}
