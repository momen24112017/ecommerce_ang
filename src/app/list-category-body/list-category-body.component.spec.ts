import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCategoryBodyComponent } from './list-category-body.component';

describe('ListCategoryBodyComponent', () => {
  let component: ListCategoryBodyComponent;
  let fixture: ComponentFixture<ListCategoryBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCategoryBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCategoryBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
