import { Component, OnInit } from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';

@Component({
  selector: 'app-list-category-body',
  templateUrl: './list-category-body.component.html',
  styleUrls: ['./list-category-body.component.css']
})
export class ListCategoryBodyComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService) { }

  categories: any[];
  
  setCategoryId(category_id){
    localStorage.setItem('category_id',category_id);
    // window.location.href="/category-body";
  }
  ngOnInit(): void {

    this.ecommerceService.getCategories().subscribe(res=>{
      this.categories=res.data;
    },ero=>{
      console.log('ero');
    });

  }

}
