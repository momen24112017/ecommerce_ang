import { Component, OnInit, ViewChild } from '@angular/core';
import { EcommerceService } from '../services/ecommerce.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService, private toastr: ToastrService, private spinner: NgxSpinnerService) { }

  user = this.ecommerceService.getuserData();
  userInfo: any;
  orders: any;

  BrendSetting = {
    title: "", banner_image:"", favicon: "", id:"", logo: "", our_story: "", email:"", phone:"",
    social_links: {facebook: "", instagram: "", google: "", twitter:"", linkedin:""},
    theme_colour: {main_colour: "", sub_colour: ""},
  };

  @ViewChild('userInfo') userInfoForm: NgForm;

  updateUserInfo(){
    this.spinner.show("UpdateSpinner");
    if(this.userInfo.name == this.user.name && this.userInfo.phone == this.user.phone && this.userInfo.address == this.user.address){
      this.toastr.error('There is no change', 'Error', {timeOut: 5000,});
      this.spinner.hide("UpdateSpinner");
      return
    }
    this.userInfo.id = this.user.id;
    this.ecommerceService.updateUserDate(this.userInfo).subscribe(res=>{
      if(res.status.code == 200){
        this.spinner.hide("UpdateSpinner");
        this.toastr.success('Your info updated login in to see updated info', 'success', {timeOut: 5000,});
        setTimeout( ()=>{this.ecommerceService.logout()}, 3000)
        return;
      }
    },ero=>{
      console.log('ero');
      this.spinner.hide("UpdateSpinner");
      this.toastr.error('', 'Error', {timeOut: 5000,});
      return;
    });

  }

  ngOnInit(): void {

    if(this.user.phone == "NULL"){
      this.user.phone = "";
    }
    if(this.user.address == "NULL"){
      this.user.address = "";
    }
    if(this.user.city == "NULL"){
      this.user.city = "";
    }
    this.userInfo = {id:"", name: this.user.name, phone: this.user.phone, address: this.user.address, city: this.user.city};

    this.ecommerceService.getOrders(this.user.id).subscribe(res=>{
      this.orders = res.data;
    },ero=>{
      console.log('ero');
    });

    this.ecommerceService.getBrendSetting().subscribe(res=>{
      this.BrendSetting = res.data;
      if(this.BrendSetting.our_story.length > 245){
        this.BrendSetting.our_story = this.BrendSetting.our_story.substr(0, 245)+'...';
      }
    },ero=>{
      console.log('ero');
    });

  }

}
