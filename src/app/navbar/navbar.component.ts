import { Component, OnInit, ViewChild, Inject, ElementRef } from '@angular/core';
import { EcommerceService } from '../services/ecommerce.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { CategoryBodyComponent } from 'src/app/category-body/category-body.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [CategoryBodyComponent],
})

export class NavbarComponent implements OnInit {
  @ViewChild('staticBackdrop') public staticBackdrop: ElementRef;
  constructor(private categoryBody: CategoryBodyComponent, @Inject(DOCUMENT) private document: HTMLDocument, private ecommerceService: EcommerceService, private toastr: ToastrService, private spinner: NgxSpinnerService, private router: Router) {
    this.getcartCount();
  }
  //Ahmed edit
  coutn = 0;
  getcartCount() {
    this.ecommerceService.cartCountMessage.subscribe(res => {
      this.coutn = res;
      console.log("my count", this.coutn);
    })
  }




  @ViewChild('objRegister') registerForm: NgForm;
  @ViewChild('objLogin') loginForm: NgForm;
  objLogin = { email: "", password: "" };
  objRegister = { name: "", email: "", password: "", confirm_password: "" };
  wrongEmail: string
  wrongPassword: string
  public user: any;

  BrendSetting = {
    title: "", banner_image: "", favicon: "", id: "", logo: "", our_story: "",
    social_links: { facebook: "", instagram: "", google: "" },
    theme_colour: { main_colour: "", sub_colour: "" }
  };

  categories: any[];
  products: any[];
  searchText = '';
  // cartLength = this.ecommerceService.updateCartLength();

  setCategoryId(category) {
    // localStorage.setItem('category',category.id);
    this.router.navigate(['/category', category.id]);
    this.ecommerceService.sendClickEvent(category.id);
    console.log('', category.id);

  }

  setProductId(product_id) {
    localStorage.setItem('product_id', product_id);
    this.router.navigateByUrl('/product-detail');
  }

  login() {
    this.spinner.show("loginSpinner");
    this.objLogin.email = this.loginForm.value.email;
    this.objLogin.password = this.loginForm.value.password;
    this.ecommerceService.login(this.objLogin).subscribe(res => {
      if (res.status.code == 4030) {
        this.toastr.error('Invalid Email', 'Error', { timeOut: 5000, });
        this.spinner.hide("loginSpinner");
        return;
      }
      if (res.status.code == 5013) {
        this.toastr.error('Wrong Password', 'Error', { timeOut: 5000, });
        this.spinner.hide("loginSpinner");
        return;
      }
      if (res.status.code == 200) {
        this.toastr.success('', 'Success', { timeOut: 5000, });
        this.spinner.hide("loginSpinner");
        localStorage.removeItem('userEmail');
        localStorage.setItem('ecommerceUserData', this.ecommerceService.encrypt(JSON.stringify(res.data)));
        this.document.location.reload();
      }
    });
  }
  @ViewChild('dismiss') dismiss;
  register(form: NgForm) {

    this.spinner.show("registerSpinner");
    this.objRegister.name = this.registerForm.value.name;
    this.objRegister.email = this.registerForm.value.email;
    this.objRegister.password = this.registerForm.value.password;
    this.objRegister.confirm_password = this.registerForm.value.confirm_password;
    if (this.objRegister.password != this.objRegister.confirm_password) {
      this.toastr.error('Password not matching', 'Error', { timeOut: 5000, });
      this.spinner.hide("registerSpinner");
      return;
    } else {
      this.ecommerceService.register(this.objRegister).subscribe(res => {
        if (res.status.code == 4016) {
          this.toastr.error('This email is already exist', 'Error', { timeOut: 5000, });
          this.spinner.hide("registerSpinner");
          return;
        }
        if (res.status.code == 200) {
          // this.toastr.success('Success');
          localStorage.setItem('userEmail', JSON.stringify(res.data.email));
          this.spinner.hide("registerSpinner");
          this.dismiss.nativeElement.click();

          this.router.navigateByUrl('/verify-your-mail-to-reg');
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error('Something went wrong')
        console.log('this is error', err);
        this.dismiss.nativeElement.click();

      });
    }

  }

  logout() {
    this.spinner.show("logoutSpinner");
    localStorage.removeItem('ecommerceUserData');
    localStorage.removeItem('userEmail');
    localStorage.clear();
    this.spinner.hide("loginSpinner");
    window.location.href = '/';
    this.router.navigateByUrl('/');

  }

  ngOnInit(): void {

    this.user = this.ecommerceService.getuserData();

    this.ecommerceService.getCategories().subscribe(res => {
      this.categories = res.data;
    }, ero => {
      console.log('ero');
    });

    this.ecommerceService.getProducts().subscribe(res => {
      this.products = res.data;
    }, ero => {
      console.log('ero');
    });

    this.ecommerceService.getBrendSetting().subscribe(res => {
      this.BrendSetting = res.data;
    }, ero => {
      console.log('ero');
    });

  }
  forgetP() {
    this.dismiss.nativeElement.click();
    this.router.navigate(['/type-email-recover-pass']);
  }

}
