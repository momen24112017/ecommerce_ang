import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'appFilter' })

export class FilterPipe implements PipeTransform {

    transform(items: any, searchText: string){
        if(searchText){
            searchText = searchText.toLocaleLowerCase();
            return items ? items.filter(item => item.name.search(new RegExp(searchText, 'i')) > -1) : [];
        }else{
            return;
        }
    }

}