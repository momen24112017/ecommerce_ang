import { Component, OnInit, ViewChild } from '@angular/core';
import { EcommerceService } from '../services/ecommerce.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-detail-body',
  templateUrl: './product-detail-body.component.html',
  styleUrls: ['./product-detail-body.component.css'],
  providers: [NgbCarouselConfig]
})

export class ProductDetailBodyComponent implements OnInit {

  constructor(private route: ActivatedRoute, config: NgbCarouselConfig, private router: Router, private ecommerceService: EcommerceService, private toastr: ToastrService) {
    config.interval = 8000;
    config.keyboard = true;
    config.pauseOnHover = true;
  }

  product = {
    id: '', name: '', price: '', image: '', quantity: 1,
  }

  images: any[];
  flag = 0;

  @ViewChild('objReview') reviewForm: NgForm;
  objReview = { user_id: "", product_id: "", review: "", rate: 0 };
  reviewSpn = false;
  currentRate = 10;

  user: any;
  cart: any;
  // cartLength =  this.ecommerceService.updateCartLength();

  BrendSetting = {
    title: "", banner_image: "", favicon: "", id: "", logo: "", our_story: "",
    social_links: { facebook: "", instagram: "", google: "" },
    theme_colour: { main_colour: "", sub_colour: "" }
  };

  productDetails = { id: "", name: "", price: "", images: "", short_desc: "" };
  relatedProducts: any[];
  relatedProductsLength = 1;
  productReviews: any[];

  setProductId(product_id) {
    // localStorage.setItem('product_id',product_id);
    this.router.navigate(['/product-detail', product_id]);
    // this.router.navigateByUrl('/product-detail');
    this.ngOnInit();
  }

  removeItem($id) {
    this.relatedProducts = this.relatedProducts.filter(item => item.id != $id);
  }

  addtoCart() {

    if (this.product.quantity == null) {
      this.toastr.error('Please enter the required quantity', 'Error', { timeOut: 5000, });
      return;
    }
    this.product.id = this.productDetails.id;
    this.product.name = this.productDetails.name;
    this.product.price = this.productDetails.price;
    this.product.image = this.productDetails.images;

    this.ecommerceService.addNewItem(this.product);
    this.cart = this.ecommerceService.getarrItem();
    // this.cartLength = this.cartLength + 1; 
  }

  saveReview() {

    this.reviewSpn = true;
    if (localStorage.getItem('ecommerceUserData') == null) {
      this.toastr.error('Please login or register first', 'Error', { timeOut: 5000, });
      this.reviewSpn = false;
      return;
    }
    this.objReview.user_id = this.user.id;
    this.objReview.product_id = localStorage.getItem('product_id');
    this.objReview.review = this.reviewForm.value.review;
    this.objReview.rate = this.currentRate;
    this.ecommerceService.saveReview(this.objReview).subscribe(res => {
      if (res.status.code == 200) {
        this.reviewSpn = false;
        this.toastr.success('Review saver successfully', 'Success', { timeOut: 5000, });
        this.ngOnInit();
      } else {
        this.reviewSpn = false;
        this.toastr.error('Try again', 'Error', { timeOut: 5000, });
      }
    }, ero => {
      console.log('ero');
    });

  }

  ngOnInit(): void {
    // this.ecommerceService.arrItem.subscribe(product => this.product);
    // this.cart = this.ecommerceService.getarrItem();
    // this.user = this.ecommerceService.getuserData();
    this.flag = 0;
    setTimeout(() => {
      console.log('product id', this.route.snapshot.params['id']);

    this.ecommerceService.getProductDetails(this.route.snapshot.params['id']).subscribe(res => {
      console.log('product details', res.data);
      this.relatedProducts = res.data.related_product;
      this.productDetails = res.data.product_details['0'];
      this.images = [res.data.product_details['0'].images];
      var secondImage = res.data.product_details['0'].second_image.substr(res.data.product_details['0'].second_image.length - 4);
      var thirdImage = res.data.product_details['0'].third_image.substr(res.data.product_details['0'].third_image.length - 4);
      if (secondImage != "NULL") {
        this.images.push(res.data.product_details['0'].second_image);
        this.flag = 1;
      }
      if (thirdImage != "NULL") {
        this.images.push(res.data.product_details['0'].third_image);
        this.flag = 1;
      }
      this.relatedProductsLength = this.relatedProducts.length
      this.productReviews = res.data.product_review;
      this.removeItem(this.route.snapshot.params['id']);
    }
    , ero => {
      console.log('error 1');
    });

    this.ecommerceService.getBrendSetting().subscribe(res => {
      this.BrendSetting = res.data;
    }, ero => {
      console.log('error getting brands');
    });

    }, 100);
    
  }

}
