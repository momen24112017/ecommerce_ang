import { Component, OnInit, ViewChild} from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';
import { NgForm } from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reset-pass',
  templateUrl: './reset-pass.component.html',
  styleUrls: ['./reset-pass.component.css']
})
export class ResetPassComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService,private router: Router) { }

  @ViewChild('objResetPassword') resetPasswordForm: NgForm;

  objResetPassword = {password:"",confirm_password:""};
  wrongPassword:string

  BrendSetting = {title: "", banner_image:"", favicon: "", id:"", logo: "", our_story: "", email:"", phone:"",
    social_links: {facebook: "", instagram: "", google: "", twitter:"", linkedin:""},
    theme_colour: {main_colour: "", sub_colour: ""},
  };
  
  ResetPassword(form: NgForm){
    this.objResetPassword.password = this.resetPasswordForm.value.password;
    this.objResetPassword.confirm_password = this.resetPasswordForm.value.confirm_password;
    
    if(this.objResetPassword.password != this.objResetPassword.confirm_password){
      this.wrongPassword = "Password not matching";
      return;
    }
  
    this.ecommerceService.resetPassword(localStorage.getItem('userEmail'), this.objResetPassword).subscribe(res=>{
      if(res.status.code == 200){
        this.router.navigateByUrl('/');
      }
    
    })
  
  }

  ngOnInit(): void {

    this.ecommerceService.getBrendSetting().subscribe(res=>{
      this.BrendSetting = res.data;
      if(this.BrendSetting.our_story.length > 245){
        this.BrendSetting.our_story = this.BrendSetting.our_story.substr(0, 245)+'...';
      }
    },ero=>{
      console.log('ero');
    });

  }

}
