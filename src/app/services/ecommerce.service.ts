import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import * as CryptoJS from 'crypto-js';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class EcommerceService {


  constructor(private http: HttpClient) {
    this.getarrItem();
  }




  encrypt(value: string): string {
    return CryptoJS.AES.encrypt(value, 'SecretKeyForEncryption&Descryption:GC3m').toString();
  }
  decrypt(textToDecrypt: string) {
    return CryptoJS.AES.decrypt(textToDecrypt, 'SecretKeyForEncryption&Descryption:GC3m').toString(CryptoJS.enc.Utf8);
  }

  userDecrypt: any;
  user: any;
  cartDecrypt: any;
  
  //Ahmed updates
  private cartCount = new BehaviorSubject(0);
  cartCountMessage = this.cartCount.asObservable();
  cart: any[] = [];
  updateCartCount() {
    this.cartCount.next(this.cart.length);
  }
  subject = new Subject<any>();
  sendClickEvent(cat) {
    this.subject.next(cat);
  }
  getClickEvent(): Observable<any>{ 
    return this.subject.asObservable();
  }

  
  // updateCartLength() {
  //   if (localStorage.getItem('arrItem')) {
  //     this.cartDecrypt = this.decrypt(localStorage.getItem('arrItem'));
  //     this.cart = JSON.parse(this.cartDecrypt);
  //     var cartLength = this.cart.length;
  //     return cartLength;
  //   } else {
  //     return 0;
  //   }
  // }

  public arrCart = new BehaviorSubject([]);
  arrItem = this.arrCart.asObservable();
  addNewItem(product: any) {
    this.updateCartCount();
    // this.arrCart.next(product);
    if (localStorage.getItem("arrItem") == undefined) {
      var cart = JSON.parse(localStorage.getItem("arrItem")) || [];
    }
    if (localStorage.getItem("arrItem") != undefined) {
      var cartDecrypt = this.decrypt(localStorage.getItem('arrItem'));
      var cart = JSON.parse(cartDecrypt) || [];
      for (var i = 0; i < cart.length; i++) {
        if (cart[i].name == product.name) {
          cart[i].quantity = cart[i].quantity + product.quantity;
          console.log(cart);
          var encryptCart = this.encrypt(JSON.stringify(cart));
          console.log(encryptCart);
          localStorage.setItem('arrItem', encryptCart);
          return;
        }
      }
    }
    cart.push(this.arrCart.value);
    var encryptCart = this.encrypt(JSON.stringify(cart));
    localStorage.setItem('arrItem', encryptCart);
  }



  getuserData() {
    if (localStorage.getItem('ecommerceUserData')) {
      this.userDecrypt = this.decrypt(localStorage.getItem('ecommerceUserData'));
      this.user = JSON.parse(this.userDecrypt);
      return this.user;
    }
  }

  logout() {
    localStorage.removeItem('ecommerceUserData');
    localStorage.removeItem('userEmail');
    localStorage.clear();
    window.location.href = '/';
  }


  getarrItem() {
    if (localStorage.getItem('arrItem')) {
      this.cartDecrypt = this.decrypt(localStorage.getItem('arrItem'));
      this.cart = JSON.parse(this.cartDecrypt);
      console.log('mycart arr', this.cart);
      this.updateCartCount();
      return this.cart;
    }
  }




  orderDecrypt: any;
  order: any;
  getOrder() {
    if (localStorage.getItem('order')) {
      this.orderDecrypt = this.decrypt(localStorage.getItem('order'));
      this.order = JSON.parse(this.orderDecrypt);
      return this.order;
    }
  }





  private API_URL = environment.API_URL;

  getCategories(): Observable<any> {
    return this.http.get(this.API_URL + '/listCategory');
  }

  getFeatureProducts(): Observable<any> {
    return this.http.get(this.API_URL + '/listFeatureProduct');
  }

  getProductDetails(product_id): Observable<any> {
    return this.http.get(this.API_URL + '/productDetails/' + product_id);
  }

  login(obj): Observable<any> {
    return this.http.post(this.API_URL + '/login', obj);
  }

  register(obj): Observable<any> {
    return this.http.post(this.API_URL + '/register', obj);
  }

  checkEmail(obj): Observable<any> {
    return this.http.post(this.API_URL + '/checkEmail', obj);
  }

  checkActive(id): Observable<any> {
    return this.http.get(this.API_URL + '/addtoCartCheck/' + id);
  }

  resendConfirmationEmail(email): Observable<any> {
    return this.http.get(this.API_URL + '/resendConformationEmail/' + email);
  }

  resetPassword(email, obj): Observable<any> {
    return this.http.post(this.API_URL + '/resetPassword/' + email, obj);
  }

  resendMailforResetPassword(email): Observable<any> {
    return this.http.get(this.API_URL + '/resendMailforResetPassword/' + email);
  }

  updateUserDate(obj): Observable<any> {
    return this.http.post(this.API_URL + '/updateUserDate', obj);
  }

  listProductAttachedtoCategory(category_id): Observable<any> {
    return this.http.get(this.API_URL + '/listProductAttachedtoCategory/' + category_id);
  }

  getProducts(): Observable<any> {
    return this.http.get(this.API_URL + '/getProducts');
  }

  pageChange(category_id, event): Observable<any> {
    return this.http.get(this.API_URL + '/listProductAttachedtoCategory/' + category_id + '?page=' + event);
  }

  listDeliveryFees(): Observable<any> {
    return this.http.get(this.API_URL + '/listDeliveryFees');
  }

  checkPromoCode($obj): Observable<any> {
    return this.http.post(this.API_URL + '/checkPromoCode', { 'promocode': $obj });
  }

  getBrendSetting(): Observable<any> {
    return this.http.get(this.API_URL + '/getBrandSetting');
  }

  createOrder($obj): Observable<any> {
    return this.http.post(this.API_URL + '/createOrder', $obj);
  }

  getOrders($id): Observable<any> {
    return this.http.get(this.API_URL + '/getOrders/' + $id);
  }

  saveReview($obj): Observable<any> {
    return this.http.post(this.API_URL + '/saveReview', $obj);
  }

}
