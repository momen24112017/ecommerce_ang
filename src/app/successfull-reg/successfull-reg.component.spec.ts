import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessfullRegComponent } from './successfull-reg.component';

describe('SuccessfullRegComponent', () => {
  let component: SuccessfullRegComponent;
  let fixture: ComponentFixture<SuccessfullRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessfullRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessfullRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
