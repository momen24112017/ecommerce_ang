import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeEmailRecoverPassComponent } from './type-email-recover-pass.component';

describe('TypeEmailRecoverPassComponent', () => {
  let component: TypeEmailRecoverPassComponent;
  let fixture: ComponentFixture<TypeEmailRecoverPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeEmailRecoverPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeEmailRecoverPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
