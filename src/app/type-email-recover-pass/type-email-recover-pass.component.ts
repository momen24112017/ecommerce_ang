import { Component, OnInit, ViewChild } from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';
import { NgForm } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import {Router} from '@angular/router';

@Component({
  selector: 'app-type-email-recover-pass',
  templateUrl: './type-email-recover-pass.component.html',
  styleUrls: ['./type-email-recover-pass.component.css']
})
export class TypeEmailRecoverPassComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService, private spinner: NgxSpinnerService,private router: Router) { }

  @ViewChild('email') recoverEmailForm: NgForm;

  wrongEmail:string
  email:string

  BrendSetting = {title: "", banner_image:"", favicon: "", id:"", logo: "", our_story: "", email:"", phone:"",
    social_links: {facebook: "", instagram: "", google: "", twitter:"", linkedin:""},
    theme_colour: {main_colour: "", sub_colour: ""},
  };

  recoverEmail(form: NgForm){
    this.spinner.show("submitSpinner");
    this.email = this.recoverEmailForm.value;
    this.ecommerceService.checkEmail(this.email).subscribe(res=>{
      if(res.status.code == 5017){
        this.wrongEmail = "This email not exist";
        this.spinner.hide("submitSpinner");
        return;
      }
      if(res.status.code == 200){
        localStorage.setItem('userEmail',res.data);
        this.spinner.hide("submitSpinner");
        this.router.navigateByUrl('/email-sent-to-recover-pass');
        return;
      }
    });
  }
  
  ngOnInit(): void {

    this.ecommerceService.getBrendSetting().subscribe(res=>{
      this.BrendSetting = res.data;
      if(this.BrendSetting.our_story.length > 245){
        this.BrendSetting.our_story = this.BrendSetting.our_story.substr(0, 245)+'...';
      }
    },ero=>{
      console.log('ero');
    });

  }
  back(){
    this.router.navigate(['/']);
  }
}


