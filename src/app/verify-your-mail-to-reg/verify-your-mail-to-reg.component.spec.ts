import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyYourMailToRegComponent } from './verify-your-mail-to-reg.component';

describe('VerifyYourMailToRegComponent', () => {
  let component: VerifyYourMailToRegComponent;
  let fixture: ComponentFixture<VerifyYourMailToRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyYourMailToRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyYourMailToRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
