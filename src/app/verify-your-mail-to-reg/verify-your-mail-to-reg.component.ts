import { Component, OnInit } from '@angular/core';
import { EcommerceService } from 'src/app/services/ecommerce.service';

@Component({
  selector: 'app-verify-your-mail-to-reg',
  templateUrl: './verify-your-mail-to-reg.component.html',
  styleUrls: ['./verify-your-mail-to-reg.component.css']
})

export class VerifyYourMailToRegComponent implements OnInit {

  constructor(private ecommerceService: EcommerceService) { }

  successMessage:string
 
  stringLength = localStorage.getItem('userEmail').length;
  email = localStorage.getItem('userEmail').substring(1, this.stringLength - 1);
  
  ResendEmail(){
    this.ecommerceService.resendConfirmationEmail(this.email).subscribe(res=>{
      if(res.status.code == 200){
        this.successMessage = "Message sent successfully please check your mail";    
        return;    
      }
    })
  }

  ngOnInit(): void {
  }

}
